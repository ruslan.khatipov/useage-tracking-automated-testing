package testing.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import javax.json.Json;
import javax.json.JsonObject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertTrue;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;


public class StepDefinitions {
    private static final String mobilusHome = "https://saemobilus-dev.sae.org/";
    Response res;
    WebDriver driver;

    @Given("^the user is logged in$")
    public void the_user_is_logged_in() {
        String path = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", path+"/src/test/java/testing/resources/webdrivers/v87/chromedriver.exe");
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("headless");
//        options.addArguments("window-size=1200x600");
//        driver = new ChromeDriver(options);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(mobilusHome);
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("m-login-box")));
        driver.findElement(By.className("m-login-box")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("userid")));
        driver.findElement(By.id("userid")).sendKeys("autotest-a2m");
        driver.findElement(By.id("password")).sendKeys("autotest-a2m");

        WebElement loginUser = driver.findElement(
            new ByChained(
                By.tagName("button"),
                By.xpath("//*[@id=\"subLogin\"]/form/div[3]/button")
            )
        );
        loginUser.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("fa-caret-down")));
        driver.findElement(By.className("fa-caret-down")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Additional Subscription Login")));
        driver.findElement(By.linkText("Additional Subscription Login")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("userid")));
        driver.findElement(By.id("userid")).sendKeys("autotest-n2z");
        driver.findElement(By.id("password")).sendKeys("autotest-n2z");
        driver.findElement(
            new ByChained(
                By.tagName("button"),
                By.xpath("//*[@id=\"subLogin\"]/form/div[3]/button")
            )
        ).click();
    }


    @When("^they search for \"([^\"]*)\"$")
    public void they_search_for(String arg1) {
        driver.findElement(By.id("advSearchTxt0")).sendKeys("Engine");
        driver.findElement(By.linkText("Search")).click();
    }

    @Then("^results are shown$")
    public void results_are_shown() throws InterruptedException {
        // TODO: verify results are shown on the page
        WebDriverWait wait = new WebDriverWait(driver, 8);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("all-search-results")));
        assertTrue(driver.getPageSource().contains("Displaying results 1-10"));
        Thread.sleep(3000);
        driver.close();
    }

    @Then("^Kabana logs the activity$")
    public void kabana_logs_the_activity() {

        // TODO:
        //  1. have the json call be a relative path call
        //  2. perhaps edits the json so the created date range can be dynamic for each test run
        //  3. maybe build the whole json body in the test instead of importing it

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String timeRangeStart = dtf.format(now.plusHours(5).minusSeconds(30));
        String timeRangeEnd = dtf.format(now.plusHours(5).plusMinutes(2));

        JsonObject object = Json.createObjectBuilder()
            .add("size", "2")
            .add("query", Json.createObjectBuilder()
                .add("bool", Json.createObjectBuilder()
                    .add("filter", Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                            .add("bool", Json.createObjectBuilder()
                                .add("should", Json.createArrayBuilder()
                                    .add(Json.createObjectBuilder()
                                        .add("bool", Json.createObjectBuilder()
                                            .add("should", Json.createArrayBuilder()
                                                .add(Json.createObjectBuilder()
                                                    .add("match", Json.createObjectBuilder()
                                                        .add("sae.subscription.id", 131192)
                                                    )
                                                )
                                            )
                                        )
                                    )
                                    .add(Json.createObjectBuilder()
                                        .add("bool", Json.createObjectBuilder()
                                            .add("should", Json.createArrayBuilder()
                                                .add(Json.createObjectBuilder()
                                                    .add("match", Json.createObjectBuilder()
                                                        .add("sae.subscription.id", 131193)
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                                .add("minimum_should_match", 1)
                            )
                        )
                        .add(Json.createObjectBuilder()
                            .add("range", Json.createObjectBuilder()
                                .add("created", Json.createObjectBuilder()
                                    .add("gte", timeRangeStart)
                                    .add("lte", timeRangeEnd)
                                    .add("format", "strict_date_optional_time")
                                )
                            )
                        )
                    )
                )
            ).build();

        var kabanaQuery = object.toString();

        RestAssured.defaultParser = Parser.JSON;
        RestAssured.useRelaxedHTTPSValidation();
        baseURI = "https://es.dev.sae.cloud:9200/saedb.usagetrackingenhancedevents/_search";
        res = (Response) given()
                .auth()
                .basic("saeuser", "testpassword")
                .header("Accept", "*/*")
                .header("Content-Type", "application/json")
                .body(kabanaQuery)
                .get()
                .thenReturn()
                .body();

        res.prettyPrint();
    }

    @Then("^the json contains user data$")
    public void the_json_contains_user_data()  {
        System.out.println(res.jsonPath().getString("hits.hits[0]._source.sae.subscription.title"));
        System.out.println(res.jsonPath().getString("hits.hits[1]._source.sae.subscription.title"));
        String subscription1 = res.jsonPath().getString("hits.hits[0]._source.sae.subscription.title");
        String subscription2 = res.jsonPath().getString("hits.hits[1]._source.sae.subscription.title");
        String responseAction1 = res.jsonPath().getString("hits.hits[0]._source.event.action");
        String responseAction2 = res.jsonPath().getString("hits.hits[1]._source.event.action");

        // TODO: verify json response here
        Assert.assertEquals("login_subscription", responseAction1);
        Assert.assertEquals("login_subscription", responseAction2);
        Assert.assertEquals("Automated Testing > Collections A to M", subscription1);
        Assert.assertEquals("Automated Testing > Collections N to Z", subscription2);
    }
}
