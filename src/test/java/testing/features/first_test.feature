Feature: Login and search
  A user logs into SAE Mobilus and performs a basic search

  Scenario: Search for "engine"
    Given the user is logged in
    When they search for "engine"
    And results are shown
    Then Kabana logs the activity
    And the json contains user data