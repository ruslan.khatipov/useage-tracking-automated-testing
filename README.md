## Usage Tracking Testing

This repo contains the automated tests that will verify the usage-tracking-service.

The tests utilize selenium for UI interaction and the Kabana API for verification of proper event tracking. 

To run the tests, simply pull the project and run `mvn test` at the root of the project.